import { TestBed } from '@angular/core/testing';

import { ApiFormService } from './api-form.service';

describe('ApiFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiFormService = TestBed.get(ApiFormService);
    expect(service).toBeTruthy();
  });
});
