import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiFormService {
  datosApi: any;

  constructor(
    private http: HttpClient
  ) {
  }

  // variables de encabezado para aceptacion peticiones
  private httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }
    )
  };

  // servicio de angular para consumo de servidor
  postGenerate(secret: any) {

    this.datosApi = {
      "secret": secret,
    };
    
    const datos = JSON.stringify(this.datosApi);

    return this.http.post(environment.API_URL,datos,this.httpOptions);

  }

  
}
