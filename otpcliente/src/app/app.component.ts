// importamos los servicios necesario para la ejecucion
import { Component, OnInit } from '@angular/core'; // libreria inicio de Angular
import { FormBuilder, FormGroup } from '@angular/forms'; // libreria para creacion de formularios
import {CookieService} from 'ngx-cookie-service';// libreria manejo de cookies en  Angular
import {ApiFormService} from './service/api-form.service';// servicio elaborado para la ejecucion del post
import {HttpClient} from '@angular/common/http';// libreria para consumo de servicios al servidor

// incio de componentes
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// inicializacion de clase 
export class AppComponent implements OnInit {

  // variables de ejecucio y entorno
  public formGroup: FormGroup;
  code: any = '';
  name: any = '';
  time: any = 0;


  constructor(
    private formBuilder: FormBuilder,
    private cookie: CookieService,
    public http: HttpClient, 
    private  apiFormService: ApiFormService) {

  }

  // funcion principal
  ngOnInit() {
    //creamos un formulario nuevo
    this.formGroup = this.formBuilder.group({
      secret: [''],
      name: ['']
    });
    
    // ejecutamos la creacion del codigo por si existe la informacion en las cookies
    this.generateCode();
    
    // ejecutamos la creacion del codigo por si existe la informacion en las cookies cada 1 segundo para ir consultando cuando se actualice
    setInterval(() => {
      this.generateCode();
    }, 1000);

  }

  onSubmit() {
    // recojemos los valores del formulario creado
    const data = this.formGroup.value;

    // asignamos los datos del formulario a las cookies
    this.cookie.set('X-Name', data.name);
    this.cookie.set('X-Secret', data.secret);

    // ejecutamos la creacion del codigo por si existe la informacion en las cookies
    this.generateCode();

    // reseteamos el formulario para que quede limpio
    this.formGroup.reset();
    
  }

  generateCode(){
    // recojer los valores almacenados en las cookies
    let name = this.cookie.get('X-Name');
    let secret = this.cookie.get('X-Secret');

    if(name && secret){
      // ejecutar el servicio establecido en el servidor localhost:3000
      this.apiFormService.postGenerate(secret).subscribe(
        result => {

          if (result['token']) {
            // asignamos las variables para que sean visibles posterior en el html
            this.code = result['token'];
            this.time = result['remaining'];
            this.name = name;

          }
        },
        error => {

          this.code = ""
          this.name = "";

          this.formGroup.reset();

      });
    }
  }

}
