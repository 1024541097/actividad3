//librerias de servidor construido en Node JS Express
const Express = require("express");// express node js
const cors = require("cors"); // habilitar conexion de cliente para evitar cors
const BodyParser = require("body-parser"); //libreria de cifrado
const Speakeasy = require("speakeasy"); //libreria de cifrado

// iniciamos la el servicio express
var app = Express();

// iniciamos una variable para poder admitir la conexion desde el cliente que estara en el localhost puerto 4100
var corsOptions = {
    origin: "http://localhost:4100"
  };

//asociamos los cors a la conexion express
app.use(cors(corsOptions));

//asociamos uso de descifrar variables para metodos post
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

// servicio para generar llave
app.post("/totp-secret", (request, response, next) => {
    var secret = Speakeasy.generateSecret({ length: 20 });
    response.send({ "secret": secret.base32 });
});

// servicio para generar codigo OTP segun la llave enviada
app.post("/totp-generate", (request, response, next) => {
    response.send({
        "token": Speakeasy.totp({
            secret: request.body.secret,
            encoding: "base32"
        }),
        "remaining": (30 - Math.floor((new Date()).getTime() / 1000.0 % 30))
    });
});


// servicio para validar la llave y codigo generado para conexion exitosa
app.post("/totp-validate", (request, response, next) => {
    response.send({
        "valid": Speakeasy.totp.verify({
            secret: request.body.secret,
            encoding: "base32",
            token: request.body.token,
            window: 0
        })
    });
});


// levantamos el servicio de node que sera escuchado en el puerto 3000 en el servidor
app.listen(3000, () => {
    console.log("Listening at :3000...");
});